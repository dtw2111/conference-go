import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    content = json.loads(body)
    send_mail(
        subject="Your presentation has been accepted!",
        message=f"{content['presenter_name']}, we're happy to tell you that your presentation {content['title']} has been accepted!",
        from_email="admin@conference.go",
        recipient_list=[content["presenter_email"]],
    )


def process_rejection(ch, method, properties, body):
    content = json.loads(body)
    send_mail(
        subject="Your presentation has been rejected.",
        message=f"{content['presenter_name']}, your presentation {content['title']} has been rejected. Better luck next time!",
        from_email="admin@conference.go",
        recipient_list=[content["presenter_email"]],
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        queue_dict = {
            "presentation_approvals": process_approval,
            "presentation_rejections": process_rejection,
        }

        for queue_name, func in queue_dict.items():
            channel.queue_declare(queue=queue_name)
            channel.basic_consume(
                queue=queue_name,
                on_message_callback=func,
                auto_ack=True,
            )

        channel.start_consuming()
    except AMQPConnectionError:
        print("presentation_mailer consumer.py could not connect to RabbitMQ")
        time.sleep(2)
